var toggleMenu = function(event){
    var hamburger = document.getElementById('icon-hamburger');
    var close = document.getElementById('icon-close-menu');
    var menu = document.getElementById('menu');
    var overlay = document.getElementById('overlay');
    overlay.style.display = overlay.style.display== 'flex' ? 'none' : 'flex';
    hamburger.hidden = !hamburger.hidden;
    close.hidden = !close.hidden;
    menu.style.display = menu.style.display== 'flex' ? 'none' : 'flex';
}